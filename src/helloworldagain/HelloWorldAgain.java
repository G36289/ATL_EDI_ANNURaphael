package helloworldagain;

/**
 *
 * @author raphaël
 */
public class HelloWorldAgain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("hello world!");
    }
    
}
